import React, { Component } from 'react';
import history from '../../history';

class Header extends Component {
    render() {
        return (
            <div>
                <div className='container-header'>
                    <div
                        className={this.props.path === "/" || this.props.path === "" ? "menu-header header-active" : "menu-header"}
                        onClick={() => history.push("/")}>
                        <p>Home</p>
                    </div>
                    <div
                        className={this.props.path === "/add-event" || this.props.path === "" ? "menu-header header-active" : "menu-header"}
                        onClick={() => history.push("/add-event")}>
                        <p>Add Event</p>
                    </div>
                    <div
                        className={this.props.path === "/dashboard" || this.props.path === "" ? "menu-header header-active" : "menu-header"}
                        onClick={() => history.push("/dashboard")}>
                        <p>Dashboard</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;