import Axios from 'axios';

const URL = 'http://localhost:8082';

const EventServices = {
    create(data) {
        return Axios.request({
            method: 'post',
            url: `${URL}/event/create`,
            data,
        })
    },
    read() {
        return Axios.request({
            method: 'get',
            url: `${URL}/event/read`,
        })
    },
}

export { EventServices };