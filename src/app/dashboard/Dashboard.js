import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../../common/asset/css/styles.css';
import { EventServices } from '../../services/EventServices';

const Dashboard = (props) => {
    const [list, setList] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [show, setShow] = useState([]);
    const [page, setPage] = useState(1);
    const [search, setSearch] = useState('');

    useEffect(() => {
        EventServices.read().then(res => {
            console.log(res);
            if (res.data !== '') {
                setList(res.data);
                setFiltered(res.data);
            }
        }).catch(err => {
            console.log(err);
            setList([]);
            setFiltered([]);
        })
    }, [])

    useEffect(() => {
        const index = (page - 1) * 5;
        let arrTemp = [];

        for (let i = index; i < index + 5; i++) {
            if (i == filtered.length) {
                break;
            }
            arrTemp.push(filtered[i])
        }

        setShow(arrTemp);
    }, [page, filtered])

    const searchHandler = () => {
        setPage(1);
        if (search !== '') {
            let arrTemp = [];

            list.map(data => {
                if (data.title.toLowerCase().includes(search.toLowerCase())
                    || data.location.toLowerCase().includes(search.toLowerCase())
                    || data.date.toLowerCase().includes(search.toLowerCase())
                    || data.participant.toLowerCase().includes(search.toLowerCase())
                    || data.note.toLowerCase().includes(search.toLowerCase())
                ) {
                    arrTemp.push(data);
                }
            })

            setFiltered(arrTemp);
        } else {
            setFiltered(list);
        }
    }

    const changePage = (isPlus) => {
        if (isPlus && page < filtered.length / 5) {
            setPage(page + 1)
        } else if (!isPlus && page > 1) {
            setPage(page - 1)
        }

    }

    const pagination = () => {
        return (
            <>
                {(() => {
                    const result = [];
                    let length = filtered.length / 5;
                    if (filtered.length % 5 > 0) {
                        length += 1;
                    }

                    for (let i = 1; i <= length; i++) {
                        result.push(<a className={page === i ? 'active' : ''} onClick={() => setPage(i)}>{i}</a>);
                    }
                    return result;
                })()}
            </>
        )
    }

    return (
        <div className='container-app'>
            <div className='container-card-dashboard'>
                <div className="search-container">
                    <input onChange={e => setSearch(e.target.value)} placeholder="Input key..." />
                    <button onClick={() => searchHandler()}>Search</button>
                </div>
                <table>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Date</th>
                        <th>Participant</th>
                        <th>Note</th>
                    </tr>
                    {show?.map(data =>
                        <tr>
                            <th>{data?.id}</th>
                            <th>{data?.title}</th>
                            <th>{data?.location}</th>
                            <th>{data?.date}</th>
                            <th>{data?.participant}</th>
                            <th>{data?.note}</th>
                        </tr>
                    )}
                </table>
                <div className="pagination">
                    <a onClick={() => changePage(false)}>{'<'}</a>
                    {pagination()}
                    <a onClick={() => changePage(true)}>{'>'}</a>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);