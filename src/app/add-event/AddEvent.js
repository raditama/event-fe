import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../../common/asset/css/styles.css';
import { EventServices } from '../../services/EventServices';

const AddEvent = (props) => {
    const [title, setTitle] = useState('');
    const [location, setLocation] = useState('');
    const [participant, setParticipant] = useState('');
    const [date, setDate] = useState('');
    const [note, setNote] = useState('');

    const _submitHandler = () => {
        if (note.length > 50
            && title !== ''
            && location !== ''
            && participant !== ''
            && date !== '') {
            const data = {
                title,
                location,
                participant,
                date,
                note,
            }

            EventServices.create(data).then(res => {
                console.log(res);
                alert("Event berhasil ditambahkan!")
            }).catch(err => {
                console.log(err);
            })
        } else {
            var msg = ""
            if (note.length <= 50) {
                msg += "Note harus lebih 50 karakter!\n"
            }
            if (title === '') {
                msg += "Title tidak boleh kosong!\n"
            }
            if (location === '') {
                msg += "Location tidak boleh kosong!\n"
            }
            if (participant === '') {
                msg += "Participant tidak boleh kosong!\n"
            }
            if (date === '') {
                msg += "Date tidak boleh kosong!\n"
            }

            alert(msg);
        }
    }

    useEffect(() => {
        setTitle(localStorage.getItem('title'));
        setLocation(localStorage.getItem('location'));
        setParticipant(localStorage.getItem('participant'));
        setDate(localStorage.getItem('date'));
        setNote(localStorage.getItem('note'));
    }, [])

    useEffect(() => {
        localStorage.setItem('title', title);
        localStorage.setItem('location', location);
        localStorage.setItem('participant', participant);
        localStorage.setItem('date', date);
        localStorage.setItem('note', note);
    }, [title, location, participant, date, note])

    return (
        <div className='container-app'>
            <div className='container-card'>
                <div className='add-evnt-card-left'>
                    <h2>Add Event</h2>

                    <p className='label-input margin-top'>Title</p>
                    <input onChange={(e) => setTitle(e.target.value)} value={title} />
                    <p className='req-input'>Field wajib diisi!</p>

                    <p className='label-input margin-top'>Location</p>
                    <input onChange={(e) => setLocation(e.target.value)} value={location} />
                    <p className='req-input'>Field wajib diisi!</p>

                    <p className='label-input margin-top'>Participant</p>
                    <input onChange={(e) => setParticipant(e.target.value)} value={participant} />
                    <p className='req-input'>Field wajib diisi!</p>

                    <p className='label-input margin-top'>Date</p>
                    <input type="date" onChange={(e) => setDate(e.target.value)} value={date} />
                    <p className='req-input'>Field wajib diisi!</p>

                    <p className='label-input margin-top'>Note</p>
                    <textarea onChange={(e) => setNote(e.target.value)} value={note} />
                    <p className='req-input'>Minimal 50 karakter!</p>

                    <p className='label-input margin-top'>Upload Picture</p>
                    <input type="file" /><br/>
                    <button onClick={_submitHandler}>Submit</button>
                </div>
                <div className='add-evnt-card-right'>
                    Image
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent);