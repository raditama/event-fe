import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import '../../common/asset/css/styles.css';
import { EventServices } from '../../services/EventServices';

const Home = (props) => {
    const [list, setList] = useState([]);

    useEffect(() => {
        EventServices.read().then(res => {
            console.log(res);
            if (res.data !== '') {
                setList(res.data);
            }
        }).catch(err => {
            console.log(err);
            setList([]);
        })
    }, [])

    return (
        <div className='container-app'>
            <div className='container-card-home'>
                {list?.map(data =>
                    <div className='card-event-content'>
                        <div className='card-event-img-container' />
                        <p>{data?.location}</p>
                        <p>{data?.title}</p>
                        <p>{data?.date}</p>
                        <p>{data?.participant}</p>
                        <p>Note: </p>
                        <p>{data?.note}</p>
                    </div>
                )}
            </div>
        </div>
    )
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(Home);